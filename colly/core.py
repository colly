import sys
import string
import logging
import csv
import re
from collections import namedtuple

from colly.exceptions import CsvImportError

import simplejson as json

class Collate(object): 
    
    _rows = {}
    headings = ""
    cols = 0 # number of columns in CSV file
    
    ''' Csv parsing and formatting.
    '''
    def __init__(self, csv_file, **options): 
        with open(csv_file, "rb") as f:
            raw = csv.reader(f)
            
            ''' Determine given columns, check against CSV.
            '''
            first_row = raw.next() # discards first row
            
            if 'headings' in options:
                headings = options['headings']
                f.seek(0) # reset iter to first row ^
            else:
                headings = first_row
                logging.info('Interpretting headings as row #1')
           
            self.headings = self.pad(first_row, headings)
            
            ''' OK, columns should be in order, ready to generate a map of the
                @csv_file.
            '''
            index, pk = {}, 0 #: set empty vars
            
            try:
                Head = namedtuple("row", ",".join(self.headings), verbose=False)
            except ValueError, err:
                raise CsvImportError(err)
            
            for row in map(Head._make, raw):
                ''' Perhaps not immediately obvious, index contains the full 
                    dataset. The "pk" (primary key) should be unique, or if 
                    not given it will be incremented.
                '''
                if hasattr(row, 'pk'):
                    index[row.pk] = row
                else:
                    pk += 1
                    index[pk] = row
        self.column = index #: saves to all to _rows, and the pk column as a set.
    
    ''' Make properties use validation & write-once only
    '''
    @property
    def column(self): return False
    
    @column.setter
    def column(self, v):
        if not self._rows:
            self._rows = v
    
    @column.getter
    def column(self): return set(self._rows) #: NB this will rtn the pk column

    ''' Utils
    '''
    def pad(self, sample, headings):
        ''' Padding rows (e.g. csv headings), trys to avoid annoying namedtuple
            with empties e.g.
                ",,film,director,"
            resolves to:
                "A,B,film,director,E"
        '''
        headings = headings or []
        if len(sample) < len(headings):
            raise CsvImportError("Given headings exceeded those in CSV")
        
        for n, col in enumerate(sample):
            auto_heading = alphabet(n)
            try:
                if re.match(r'^\s|,|$', headings[n]):
                    headings[n] = auto_heading
            except IndexError:
                headings.append(auto_heading)

        logging.info("CSV columns given headings: %s" % (headings))
        return headings

    def get_row(self, pk):
        return self._rows[pk]

    ''' Rehashing/ formatting
    '''
    def as_json(self):
        m = {}
        for i in self.column:
            m[i] = self._rows[i]._asdict() #: turn _rows (namedtuple) into dict => dump.
        return json.dumps(m)

''' Helpers
'''
def alphabet(n):
    ''' returns letter of alphabet at 'n', then increments A2, B2, C2 eg:
    >>> assert (alphabet(0), alphabet(25)) == ('A', 'Z')
    >>> assert alphabet(26) == 'A2'
    >>> assert alphabet(42) == 'Q2'
    >>> assert alphabet(52) == 'A3'
    '''
    if n < 26:
        return string.uppercase[n]
    
    diff = float(n) / 26
    step = int(round((diff % 1) * 26))
     
    return "%s%s" % (
      string.uppercase[step],
      str(int(diff+1)) #: number of iterations of alphabet, start at '2'
    )

if __name__ == "__main__":
    import doctest
    doctest.testmod()
